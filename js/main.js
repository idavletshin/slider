$(document).ready(function () {

    $('.js-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: '<button class="arrow arrow--prev"></button>',
        nextArrow: '<button class="arrow arrow--next"></button>'
    });

})